package fr.afpa.festival.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class FestivalsBDD {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "festivals.db";

    private static final String TABLE_FESTIVALS = "table_festivals";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_TITRE = "Titre";
    private static final int NUM_COL_TITRE = 1;

    private SQLiteDatabase bdd;

    private MaBaseSQLite maBaseSQLite;

    public FestivalsBDD(Context context){
        //On crée la BDD et sa table
        maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertFestival(MyFestival festival){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        // ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_TITRE, festival.getTitre());
        //insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_FESTIVALS, null, values);
    }

    public int updateFestival(int id, MyFestival festival){
        //La mise à jour d'un festival dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quel festival on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_TITRE, festival.getTitre());
        return bdd.update(TABLE_FESTIVALS, values, COL_ID + " = " +id, null);
    }

    /*public int removeFestivalWithID(int id){
        //Suppression d'un festival de la BDD grâce à l'ID
        return bdd.delete(TABLE_FESTIVALS, COL_ID + " = " +id, null);
    }*/

    public MyFestival getFestivalWithTitre(String titre){
        //Récupère dans un Cursor les valeurs correspondant à un festival contenu dans la BDD (ici on sélectionne le festival grâce à son titre)
        Cursor c = bdd.query(TABLE_FESTIVALS, new String[] {COL_ID, COL_TITRE}, COL_TITRE + " LIKE \"" + titre +"\"", null, null, null, null);
        return cursorToFestival(c);
    }

    //Cette méthode permet de convertir un cursor en un festival
    private MyFestival cursorToFestival(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un festival
        MyFestival festival = new MyFestival();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        festival.setId(c.getInt(NUM_COL_ID));
        festival.setTitre(c.getString(NUM_COL_TITRE));
        //On ferme le cursor
        c.close();

        //On retourne le festival
        return festival;
    }
}
