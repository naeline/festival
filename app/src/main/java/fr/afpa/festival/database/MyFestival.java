package fr.afpa.festival.database;

public class MyFestival {

    private int id;
    private String titre;

    public MyFestival(){}

    public MyFestival(String titre){
        this.titre = titre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String toString(){
        return "ID : "+id+"\nTitre : "+titre;
    }
}
