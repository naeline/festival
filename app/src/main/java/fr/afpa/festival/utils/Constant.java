package fr.afpa.festival.utils;

import org.jsoup.Jsoup;

public class Constant {


    public static final String URL_FESTIVAL = "https://opendata.paris.fr/api/records/1.0/search/?dataset=que-faire-a-paris-&facet=category&facet=tags&facet=address_zipcode&facet=address_city&facet=pmr&facet=blind&facet=deaf&facet=access_type&facet=price_type&refine.address_city=Paris&refine.category=%C3%89v%C3%A9nements-%3EFestival+%2F+Cycle";

    // methode pour transformer du html en text grace a Jsoup
    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }
}
