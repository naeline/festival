package fr.afpa.festival.models;


import java.io.Serializable;
import java.util.List;

public class Festival implements Serializable {
    public List<Records> getRecords() {
        return records;
    }

    private List<Records> records;

    public class Records implements Serializable {
        private Fields fields;

        public Fields getFields() {
            return fields;
        }
    }

    public class Fields implements Serializable {
        private String address_zipcode;
        private String address_street;
        private String price_details;
        private String contact_url;
        private String contact_phone;
        private String title;
        private String cover_url;
        private String description;
        private String tags;
        private String lead_text;

        public String getDescription() {
            return description;
        }

        public String getTags() {
            return tags;
        }

        public String getLead_text() {
            return lead_text;
        }

        public String getCover_url() {
            return cover_url;
        }

        public String getAddress_zipcode() {
            return address_zipcode;
        }

        public String getAddress_street() {
            return address_street;
        }

        public String getPrice_details() {
            return price_details;
        }

        public String getContact_url() {
            return contact_url;
        }

        public String getContact_phone() {
            return contact_phone;
        }

        public String getTitle() {
            return title;
        }
    }

}
