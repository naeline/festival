package fr.afpa.festival.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fr.afpa.festival.R;
import fr.afpa.festival.models.Festival;

public class FestivalAdapter extends ArrayAdapter<Festival.Records> {

    public FestivalAdapter(Context context, int resource, List<Festival.Records> objects) {
        super(context, resource, objects);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_festival, null);

        TextView textViewName = convertView.findViewById(R.id.textViewName);
        TextView textViewAdress = convertView.findViewById(R.id.textViewAdress);
        TextView textViewArrondissement = convertView.findViewById(R.id.textViewArrondissement);
        ImageView imageViewCover = convertView.findViewById(R.id.imageViewCover);

        Festival.Records item = getItem(position);

        textViewName.setText(item.getFields().getTitle());
        textViewAdress.setText(item.getFields().getAddress_street());
        textViewArrondissement.setText(item.getFields().getAddress_zipcode());

        Picasso.get().load(item.getFields().getCover_url())
                .fit().centerCrop()
                .into(imageViewCover);

        return convertView;
    }
}
