package fr.afpa.festival.ui.details;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import fr.afpa.festival.R;
import fr.afpa.festival.database.FestivalsBDD;
import fr.afpa.festival.database.MyFestival;
import fr.afpa.festival.models.Festival;
import fr.afpa.festival.utils.Constant;

public class DetailsActivity extends AppCompatActivity {

    private ImageView imageViewCover;
    private TextView textViewTitle;
    private TextView textViewTags;
    private TextView textViewDescribe;
    private Button buttonUrl;
    private Button buttonPhone;
    private Festival.Records object;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        imageViewCover = (ImageView) findViewById(R.id.imageViewCover);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewTags = (TextView) findViewById(R.id.textViewTags);
        textViewDescribe = (TextView) findViewById(R.id.textViewDescribe);
        buttonUrl = (Button) findViewById(R.id.buttonUrl);
        buttonPhone = (Button) findViewById(R.id.buttonPhone);

        if(getIntent().getExtras() != null) {
            object = (Festival.Records) getIntent().getExtras().get("festival");


            // on utilise Jsoup et la constante qu on a mise dans la class Constant
            textViewDescribe.setText(Constant.html2text(object.getFields().getDescription()));
            textViewTitle.setText(object.getFields().getLead_text());
            textViewTags.setText(object.getFields().getTags());
            buttonPhone.setText("Téléphone");
            buttonUrl.setText("Site web");
            Picasso.get().load(object.getFields().getCover_url())
                    .fit().centerCrop()
                    .into(imageViewCover);
        }

    }

    public void showWebSite(View view) {
        Intent intentUrl = new Intent(Intent.ACTION_VIEW);      // INTENT IMPLICITE
                // constante toujours en majuscule et separé par underscore. Ici c est pour aller voir le site

        intentUrl.setData(Uri.parse(object.getFields().getContact_url()));

        startActivity(intentUrl);
    }

    public void call(View view) {
        Intent intentPhone = new Intent(Intent.ACTION_CALL);

        intentPhone.setData(Uri.parse(object.getFields().getContact_phone()));

        // DEMANDE DE PERMISSION APPEL
        if (ContextCompat.checkSelfPermission(DetailsActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{ Manifest.permission.CALL_PHONE }, 100);
            }   //exécuté qu a partir de l api 23 grace a alt entree
        }   else {
            startActivity(intentPhone);
        }
    }

    public void showFavorite(View view) {


        //Création d'une instance de ma classe FesstivalBDD
        FestivalsBDD festivalBdd = new FestivalsBDD(this);

        //Création d'un festival
        MyFestival festival = new MyFestival(object.getFields().getTitle());

        //On ouvre la base de donnée pour écrire dedans
        festivalBdd.open();
        //On insère le festival que l'on vient de créer
        festivalBdd.insertFestival(festival);

        //Pour vérifier que l'on a bien créé notre festival dans la BDD
        //on extrait le festival de la BDD grâce au titre que l'on a créé précédemment
        MyFestival festivalFromBdd = festivalBdd.getFestivalWithTitre(festival.getTitre());
        //Si un festival est retourné (donc si le festival à bien été ajouté à la BDD)
        if(festivalFromBdd != null){
            //On affiche les infos du festival dans un Toast
            Toast.makeText(this, "Le festival a bien été ajputé en base", Toast.LENGTH_LONG).show();
        }else {
            //On affiche les infos du festival dans un Toast
            Toast.makeText(this, "Le festival n'a pas été ajouté en base", Toast.LENGTH_LONG).show();
        }

        festivalBdd.close();
    }
}
