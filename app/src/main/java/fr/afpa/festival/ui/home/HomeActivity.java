package fr.afpa.festival.ui.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.List;

import fr.afpa.festival.R;
import fr.afpa.festival.adapter.FestivalAdapter;
import fr.afpa.festival.models.Festival;
import fr.afpa.festival.ui.details.DetailsActivity;
import fr.afpa.festival.utils.Constant;
import fr.afpa.festival.utils.FastDialog;
import fr.afpa.festival.utils.Network;

public class HomeActivity extends AppCompatActivity {

    private ListView listViewData;
    private FestivalAdapter adapter;
    private List<Festival.Records> records;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        listViewData = (ListView) findViewById(R.id.listViewData);

        fetchFestival();

        cliqueDetail();

    }

    private void cliqueDetail() {

        listViewData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               final Festival.Records objetFestival = records.get(position);

                Intent intentDetailFestival = new Intent(HomeActivity.this, DetailsActivity.class);

                intentDetailFestival.putExtra("festival", objetFestival);
                startActivity(intentDetailFestival);
            }
        });
    }

    private void fetchFestival() {

        if (Network.isNetworkAvailable(HomeActivity.this)) {

            RequestQueue queue = Volley.newRequestQueue(this);
            String url = Constant.URL_FESTIVAL;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String json) {

                            Gson myGson = new Gson();

                            Festival myFestival = myGson.fromJson(json, Festival.class);
                            records = myFestival.getRecords();
                            adapter = new FestivalAdapter(HomeActivity.this, R.layout.item_festival, records);
                            listViewData.setAdapter(adapter);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Handle error
                        }
                    });

// Add the request to the RequestQueue.
            queue.add(stringRequest);
        } else {
            FastDialog.showDialog(HomeActivity.this, FastDialog.SIMPLE_DIALOG, "Vous devez être connecté");
        }
    }
}
